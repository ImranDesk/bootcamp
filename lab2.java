import java.util.Scanner;

class Lab2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter first number: ");
        String fno = sc.nextLine();
        System.out.println("Enter second number: ");
        String sno = sc.nextLine();
        System.out.println("Before swapping: " + fno + " , " + sno);

        String temp;
        temp = fno;
        fno = sno;
        sno = temp;

        System.out.println("After swapping: " + fno + " , " + sno);
    }
}