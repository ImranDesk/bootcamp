n, m = int(input('Enter starting value: ')), int(input('Enter ending value: '))  
sum = 0  
for i in range(n, m + 1):
    flag = False
    for j in range(2, i):
        if i % j == 0:
            flag = True  # composite number
    if flag == False and i != 1:  # if isn't composite number
        sum += i
print(sum)